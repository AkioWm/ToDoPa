import org.update4j.Configuration;
import org.update4j.FileMetadata;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.lang.String.format;

public class BootstrapApplication {

    public static void main(final String[] args) throws IOException {
        final var ciJobId = System.getenv("CI_JOB_ID");

        final var configuration = Configuration.builder()
                .baseUri(format("https://gitlab.com/AkioWm/ToDoPa/-/jobs/%s/artifacts/raw",ciJobId))

                .basePath("${user.home}/Alliodesk/services/ToDoList/")
                .property("default.launcher.main.class", "application.fr.esgi.pa.alliodesk.todolist.Main")
                .file(FileMetadata
                        .readFrom("application/target/application-0.1.0-SNAPSHOT.jar")
                        .path("ToDoList.jar")
                        .classpath()
                )
                .build();

        Files.writeString(Path.of("bootstrap/target/ToDoList.xml"), configuration.toString());
    }
}
