package fr.esgi.pa.alliodesk.ToDoList;

import com.google.gson.Gson;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import java.time.LocalDate;

public class ToDoListController {
    private static Gson gson = new Gson();
    ObservableList<LocalEvent> list = FXCollections.observableArrayList();
    @FXML
    private DatePicker datePicker;

    @FXML
    private TextField descriptionTestField;

    @FXML
    private ListView<LocalEvent> eventList;

    @FXML
    public void initialize() {
        datePicker.setValue(LocalDate.now());
        LocalEvent[] todos = LocalEvent.todolistReadFromFile();
        if (todos != null) {
            list.addAll(todos);
            eventList.setItems(list);
        }

    }

    @FXML
    private void addEvent(ActionEvent event) {
        LocalEvent todo = new LocalEvent(descriptionTestField.getText(), datePicker.getValue());
        list.add(todo);
        LocalEvent.todolistWriteToFile(list);
        eventList.setItems(list);
        descriptionTestField.setText("");
    }

    @FXML
    void clearEvent(ActionEvent event) {
        LocalEvent.clearFile();
        eventList.getItems().clear();
    }
}
